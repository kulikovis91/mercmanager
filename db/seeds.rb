# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

alpha = Team.create(name: 'Alpha')
Merc.create(first_name: 'Brown', last_name: 'Hedgehog', callsign: 'Maker', bio: "empty", combat_performance: 19, team: alpha)
Merc.create(first_name: 'White', last_name: 'Razorback', callsign: 'Fixer', bio: "empty", combat_performance: 19, team: alpha)
Merc.create(first_name: 'Grey', last_name: 'Fox', callsign: 'Rose', bio: "empty", combat_performance: 999, team: alpha)
Merc.create(first_name: 'Purple', last_name: 'Hippo', callsign: 'Braker', bio: "empty", combat_performance: 19, team: alpha)

#birthday = Album.create(title: "Birthday", image: "http://s3.amazonaws.com/codecademy-content/courses/rails-auth/img/birthday-1.jpg")
#Photo.create(caption: "Turning 21", image: "http://s3.amazonaws.com/codecademy-content/courses/rails-auth/img/birthday-2.jpg", album_id: birthday.id)
#Photo.create(caption: "Bringing in the style", image: "http://s3.amazonaws.com/codecademy-content/courses/rails-auth/img/birthday-3.jpg", album_id: birthday.id)
#Photo.create(caption: "Dinner with friends", image: "http://s3.amazonaws.com/codecademy-content/courses/rails-auth/img/birthday-4.jpg", album_id: birthday.id)
#Photo.create(caption: "Cake", image: "http://s3.amazonaws.com/codecademy-content/courses/rails-auth/img/birthday-5.jpg", album_id: birthday.id)
#      t.string :callsign
#      t.string :bio
#      t.int :combat_performance