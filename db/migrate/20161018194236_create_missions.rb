class CreateMissions < ActiveRecord::Migration
  def change
    create_table :missions do |t|
      t.string :name
      t.string :brief
      t.integer :difficulty
      t.timestamps null: false
    end
  end
end
