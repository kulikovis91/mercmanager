class CreateMercs < ActiveRecord::Migration
  def change
    create_table :mercs do |t|
      t.string :first_name
      t.string :last_name
      t.string :callsign
      t.string :bio
      t.integer :combat_performance
      t.belongs_to :team, index: true
      t.timestamps null: false
    end
  end
end
