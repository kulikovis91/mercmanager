class Team < ActiveRecord::Base
    has_many :mercs
    belongs_to :user
end
